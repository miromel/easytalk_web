$(document).ready(function () {
    console.log("EasyTalk is run!");
});


//---- Слайдер звука ----//

var rangeSlider = function () {
    var slider = $(".range-slider"),
        range = $(".range-slider__range"),
        value = $(".range-slider__value");

    slider.each(function () {
        value.each(function () {
            var value = $(this)
                .prev()
                .attr("value");
            $(this).html(value);
        });

        range.on("input", function () {
            $(this)
                .next(value)
                .html(this.value);
        });
        range.change(function () {
            var val = ($(this).val() - $(this).attr("min")) / ($(this).attr("max") - $(this).attr("min"));
            $(this).css(
                "background-image",
                "-webkit-gradient(linear, left top, right top, " +
                "color-stop(" +
                val +
                ", #47DFAC), " +
                "color-stop(" +
                val +
                ", #d7dcdf)" +
                ")"
            );
        });
    });
};
rangeSlider();
//----//

//---- Кнопка MUTE ----//
let muteButton = document.querySelector("input[name=mute]");

muteButton.addEventListener( 'change', function() {
    if(this.checked) {
        console.log('Звук выключен');
        
    } else {
        console.log('Звук снова включен');
        
    }
});
//----//




